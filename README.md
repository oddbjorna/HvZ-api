# Humans vs. Zombies

Humans vs. Zombies (HvZ) is a game of tag played at schools, camps, neighborhoods, libraries, and conventions around the world.
The game simulates the exponential spread of a fictional zombie infection through a population.
This application creates a base for playing HvZ in the real world where the players can manage their own states. They can see whether they are humans or zombies,
join a squad of other humans or zombies depending on faction, leave check-in markers for your squad to show where you are on the map. 

The application can be found at: [https://humans-vs-zombies-react.herokuapp.com/login]

The frontend source code with more details about the application can be found at: [https://gitlab.com/oddbjorna/humans-vs-zombies-frontend]


## API

The API is guarded by JSON web tokens (JWT) on certain endpoints, such as some post requests, delete requests, and put requests. The JWT requires the role
"Admin" for the restricted endpoints.

The API documentation can be found at: [https://gitlab.com/oddbjorna/HvZ-api/-/blob/master/databaseDiagram.PNG]

The database diagram can be found at: [https://gitlab.com/oddbjorna/HvZ-api/-/blob/master/API_Documentation.pdf]

## Contributors:

Oddbjørn Almenning, 
Ludvig Ånestad, 
Sunniva Stolt-Nielsen, 
Nanfrid Idsø


## Limitations:
The application does not provide rate limiting or two factor authentication.

## Installation:
Backend installation

Clone the source code: git clone https://gitlab.com/oddbjorna/HvZ-api
