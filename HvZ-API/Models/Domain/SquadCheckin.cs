﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Models.Domain
{
    [Table("SquadCheckIn")]
    public class SquadCheckin
    {
        // Fields
        public int Id { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public DateTime StartTime { get; set; }
        [Required]
        public DateTime EndTime { get; set; }
        [Required]
        public double Latitude { get; set; }
        [Required]
        public double Longitude { get; set; }

        // Relationships
        public int? GameId { get; set; }
        public Game Game { get; set; }

        public int? SquadId { get; set; }
        public Squad Squad { get; set; }

        public int? SquadMemberId { get; set; }
        public SquadMember SquadMember { get; set; }



    }
}
