﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Models.Domain
{
    [Table("Player")]
    public class Player
    {
        // Fields
        public int Id { get; set; }
        [Required]
        public bool IsHuman { get; set; }
        [Required]
        public bool IsPatientZero { get; set; }
        [Required]
        public string BiteCode { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public int? Rank { get; set; }

        // Relationships
        public int? UserId { get; set; }
        public User User { get; set; }

        public int? GameId { get; set; }
        public Game Game { get; set; }
    }
}
