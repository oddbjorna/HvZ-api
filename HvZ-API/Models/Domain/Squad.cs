﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Models.Domain
{
    [Table("Squad")]
    public class Squad
    {
        // Fields
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [Required]
        public bool IsHuman { get; set; }
        public int? Deceased { get; set; }

        // Relationships
        public int? GameId { get; set; }
        public Game Game { get; set; }

        public ICollection<SquadMember> SquadMembers { get; set; }


    }
}
