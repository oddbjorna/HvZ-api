﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Models.Domain
{
    [Table("Chat")]
    public class Chat
    {
        // Fields
        public int Id { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        [MaxLength(255)]
        public string Message { get; set; }

        [Required]
        public bool IsHumanGlobal { get; set; }
        [Required]
        public bool IsZombieGlobal { get; set; }
        [Required]
        public DateTime ChatTime { get; set; }

        // Relationships
        public int? GameId { get; set; }
        public Game Game { get; set; }

        public int? UserId { get; set; }
        public int? SquadMemberId { get; set; }
        public User user { get; set; }

        public int? SquadId { get; set; }

        public Squad Squad { get; set; }

    }
}
