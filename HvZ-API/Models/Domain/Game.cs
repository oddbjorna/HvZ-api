﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Models.Domain
{
    [Table("Game")]
    public class Game
    {
        // Fields
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [Required]
        public string GameState { get; set; }
        [Required]
        public double NwLat { get; set; }
        [Required]
        public double NwLng { get; set; }
        [Required]
        public double SeLat { get; set; }
        [Required]
        public double SeLng { get; set; }
        [Required]
        public string Description { get; set; }


    }
}
