﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Models.Domain
{
    [Table("SquadMember")]
    public class SquadMember
    {
        // Fields
        public int Id { get; set; }

        public string Username { get; set; }

        public bool IsHuman { get; set; }

        // Relationships
        public int? GameId { get; set; }
        public Game Game { get; set; }

        public int? SquadId { get; set; }
        public Squad Squad { get; set; }

        public int PlayerId { get; set; }
        public Player Player { get; set; }
    }
}
