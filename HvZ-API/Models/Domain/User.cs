﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Models.Domain
{
    [Table("User")]
    public class User
    {
        // Fields
        public int Id { get; set; }
        [Required]
        public string Username { get; set; }

    }
}
