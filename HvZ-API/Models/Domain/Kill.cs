﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Models.Domain
{
    [Table("Kill")]
    public class Kill
    {
        // Fields
        public int Id { get; set; }
        [Required]
        public DateTime TimeOfDeath { get; set; }
        [Required]
        public double Latitude { get; set; }
        [Required]
        public double Longitude { get; set; }

        // Relationships
        public int? KillerId { get; set; }

        public string VictimName { get; set; }
        public User Victim { get; set; }

        public int? GameId { get; set; }
        public Game Game { get; set; }

    }
}
