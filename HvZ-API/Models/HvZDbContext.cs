﻿using Microsoft.EntityFrameworkCore;
using HvZ_API.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics.CodeAnalysis;

namespace HvZ_API.Models
{
    public class HvZDbContext : DbContext
    {
        // Tables
        public DbSet<Chat> Chats { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Kill> Kills { get; set; }
        public DbSet<Mission> Missions { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Squad> Squads { get; set; }
        public DbSet<SquadCheckin> Squad_Checkins { get; set; }
        public DbSet<SquadMember> Squad_Members { get; set; }
        public DbSet<User> Users { get; set; }

        public HvZDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
        }
    }

}
