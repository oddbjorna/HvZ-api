﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Models.DTO.Squad
{
    public class SquadEditDTO
    {

        public int Id { get; set; }
        public string Name { get; set; }

        public bool IsHuman { get; set; }

        public int? Deceased { get; set; }

        public int? GameId { get; set; }

        public ICollection<int> SquadMembers { get; set; }
    }
}
