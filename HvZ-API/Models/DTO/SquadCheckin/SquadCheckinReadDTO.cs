﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Models.DTO.Squad_Checkin
{
    public class SquadCheckinReadDTO
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public int? GameId { get; set; }

        public int? SquadId { get; set; }

        public int SquadMemberId { get; set; }
    }
}
