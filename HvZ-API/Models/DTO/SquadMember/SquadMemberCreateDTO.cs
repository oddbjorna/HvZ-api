﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Models.DTO.Squad_Member
{
    public class SquadMemberCreateDTO
    {
        public bool IsHuman { get; set; }
        public string Username { get; set; }
        public int? GameId { get; set; }
        public int? SquadId { get; set; }
        public int PlayerId { get; set; }
    }
}
