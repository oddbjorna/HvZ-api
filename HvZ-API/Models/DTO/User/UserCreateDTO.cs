﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Models.DTO.User
{
    public class UserCreateDTO
    {
        public string Username { get; set; }
    }
}