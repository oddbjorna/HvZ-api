﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Models.DTO.Game
{
    public class GameReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string GameState { get; set; }
        public double NwLat { get; set; }
        public double NwLng { get; set; }
        public double SeLat { get; set; }
        public double SeLng { get; set; }
        public string Description { get; set; }
    }
}
