﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Models.DTO.Chat
{
    public class ChatReadDTO
    {
        public string Username { get; set; }
        public int Id { get; set; }
        public string Message { get; set; }
        public bool IsHumanGlobal { get; set; }
        public bool IsZombieGlobal { get; set; }
        public DateTime ChatTime { get; set; }
        public int? GameId { get; set; }
        public int? UserId { get; set; }
        public int? SquadMemberId { get; set; }
        public int? SquadId { get; set; }
    }
}
