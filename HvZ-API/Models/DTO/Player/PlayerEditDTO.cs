﻿using HvZ_API.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Models.DTO.Player
{
    public class PlayerEditDTO
    {
        public int Id { get; set; }

        public bool IsHuman { get; set; }

        public bool IsPatientZero { get; set; }

        public string BiteCode { get; set; }

        public int? UserId { get; set; }

        public int? GameId { get; set; }
        public int? Rank { get; set; }
        public string Username { get; set; }

    }
}
