﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Models.DTO.Kill
{
    public class KillCreateDTO
    {
        public DateTime TimeOfDeath { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int? KillerId { get; set; }
        public string VictimName { get; set; }
        public int? GameId { get; set; }
    }
}
