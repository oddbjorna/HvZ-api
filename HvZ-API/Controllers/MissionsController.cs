﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HvZ_API.Models;
using HvZ_API.Models.Domain;
using System.Net.Mime;
using AutoMapper;
using HvZ_API.Services.MissionService;
using HvZ_API.Models.DTO.Mission;
using Microsoft.AspNetCore.Authorization;

namespace HvZ_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Authorize]
    public class MissionsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IMissionService _missionService;

        public MissionsController(IMapper mapper, IMissionService missionService)
        {
            _mapper = mapper;
            _missionService = missionService;
        }

        // GET: api/Missions
        /// <summary>
        /// Gets all the missions in the database
        /// </summary>
        /// <returns>Returns a list of missions</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MissionReadDTO>>> GetMissions()
        {
            return _mapper.Map<List<MissionReadDTO>>(await _missionService.GetMissionsAsync());
        }

        // GET: api/Missions/5
        /// <summary>
        /// Gets a specific mission based on an id provided by a parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns MissionReadDTO on success, 404 not found on fail</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MissionReadDTO>> GetMission(int id)
        {
            try
            {
                var mission = _mapper.Map<MissionReadDTO>(await _missionService.GetMissionAsync(id));
                return mission;
            }catch
            {
                return NotFound();
            }
        }

        // PUT: api/Missions/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Updates the attributes of an existing mission based on provided id by a parameter
        /// </summary>
        /// <param name="id"></param>
        /// <param name="mission"></param>
        /// <returns>Returns 204 No Content on success, 400 Bad Request or 404 Not Found on fail</returns>
        [HttpPut("{id}")]
        [Authorize("Admin")]
        public async Task<IActionResult> PutMission(int id, MissionEditDTO mission)
        {
            if (id != mission.Id || id != mission.GameId)
            {
                return BadRequest();
            }

            if (!_missionService.MissionExists(id))
            {
                return NotFound();
            }

            Mission domainMission = _mapper.Map<Mission>(mission);

            await _missionService.PutMissionAsync(domainMission);

            return NoContent();
        }

        // POST: api/Missions
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Creates a new Mission in the database
        /// </summary>
        /// <param name="mission"></param>
        /// <returns>Returns 201 Created mission, returns 404 Not found on fail</returns>
        [HttpPost]
        [Authorize("Admin")]
        public async Task<ActionResult<Mission>> PostMission(MissionCreateDTO mission)
        {
            try
            {
                Mission domainMission = _mapper.Map<Mission>(mission);
                await _missionService.PostMissionAsync(domainMission);

                return CreatedAtAction("GetMission", new { id = domainMission.Id }, domainMission);
            }
            catch
            {
                return NotFound();
            }
            
        }

        // DELETE: api/Missions/5
        /// <summary>
        /// Deletes a mission from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns 204 No Content, returns 404 Not Found on fail</returns>
        [HttpDelete("{id}")]
        [Authorize("Admin")]
        public async Task<IActionResult> DeleteMission(int id)
        {
            try
            {
                var mission = await _missionService.GetMissionAsync(id);
                await _missionService.DeleteMission(mission);

                return NoContent();
            }
            catch
            {
                return NotFound();
            }
        }
    }
}
