﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HvZ_API.Models;
using HvZ_API.Models.Domain;
using System.Net.Mime;
using HvZ_API.Services.GameService;
using AutoMapper;
using HvZ_API.Models.DTO.Game;
using HvZ_API.Models.DTO.Chat;
using Microsoft.AspNetCore.Authorization;

namespace HvZ_API.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Authorize]
    public class GamesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IGameService _gameService;

        public GamesController(IMapper mapper, IGameService gameService)
        {
            _mapper = mapper;
            _gameService = gameService;
        }

        // GET: api/Games
        /// <summary>
        /// Gets all the games from the database
        /// </summary>
        /// <returns>Returns a list of games</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GameReadDTO>>> GetGames()
        {
            return _mapper.Map<List<GameReadDTO>>(await _gameService.GetGamesAsync());
        }

        // GET: api/Games/5
        /// <summary>
        /// Gets a specific game based on an id provided by a parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns GameReadDTO on success, 404 Not found on fail</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<GameReadDTO>> GetGame(int id)
        {
            try
            {
                var game = _mapper.Map<GameReadDTO>(await _gameService.GetGameAsync(id));
                return game;
            }
            catch
            {
                return NotFound();
            }
        }

        // GET: api/Games/id/chat
        /// <summary>
        /// Gets a specific chat from a game based on an id provided by a parameter 
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns ChatReadDTO on success, 404 Not found on fail</returns>
        [HttpGet("{id}/chat")]
        public async Task<ActionResult<IEnumerable<ChatReadDTO>>> GetChat(int id)
        {
            try
            {
                var game = _mapper.Map<GameReadDTO>(await _gameService.GetGameAsync(id));
                var chat = _mapper.Map<List<ChatReadDTO>>(await _gameService.GetChat(id));
                return chat;
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpGet("{id}/chat/Human")]
        public async Task<ActionResult<IEnumerable<ChatReadDTO>>> GetHumanChat(int id)
        {
            try
            {
                var game = _mapper.Map<GameReadDTO>(await _gameService.GetGameAsync(id));
                var chat = _mapper.Map<List<ChatReadDTO>>(await _gameService.GetHumanFactionChat(id));
                return chat;
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpGet("{id}/chat/Zombie")]
        public async Task<ActionResult<IEnumerable<ChatReadDTO>>> GetZombieChat(int id)
        {
            try
            {
                var game = _mapper.Map<GameReadDTO>(await _gameService.GetGameAsync(id));
                var chat = _mapper.Map<List<ChatReadDTO>>(await _gameService.GetZombieFactionChat(id));
                return chat;
            }
            catch
            {
                return NotFound();
            }
        }

        // PUT: api/Games/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Updates the attributes of an existing game based on id provided by a parameter.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="game"></param>
        /// <returns>Returns 204 No Content on success, 400 Bad Request or 404 Not Found on fail</returns>
        [HttpPut("{id}")]
        [Authorize("Admin")]
        public async Task<IActionResult> PutGame(int id, GameEditDTO game)
        {
            if (id != game.Id)
            {
                return BadRequest();
            }

            if (!_gameService.GameExists(id))
            {
                return NotFound();
            }

            Game domainGame = _mapper.Map<Game>(game);

            await _gameService.PutGameAsync(domainGame);

            return NoContent();
        }

        // POST: api/Games
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Creates a new game in the database
        /// </summary>
        /// <param name="game"></param>
        /// <returns>Returns 201 Created game</returns>
        [HttpPost]
        [Authorize("Admin")]
        public async Task<ActionResult<Game>> PostGame(GameCreateDTO game)
        {
            Game domainGame = _mapper.Map<Game>(game);
            await _gameService.PostGameAsync(domainGame);

            return CreatedAtAction("GetGame", new { id = domainGame.Id }, domainGame);
        }

        // POST: api/Games/chat
        /// <summary>
        /// Creates a chat for a specific game and saves it to the database based on id provided by a parameter
        /// </summary>
        /// <param name="id"></param>
        /// <param name="chat"></param>
        /// <returns>Returns 204 No Content on success, 400 Bad Request or 404 Not Found on fail</returns>
        [HttpPost("{id}/chat")]
        public async Task<ActionResult<Chat>> PostChat(int id, ChatCreateDTO chat)
        {

            if (id != chat.GameId)
            {
                return BadRequest();
            }

            try
            {
                Chat domainChat = _mapper.Map<Chat>(chat);
                await _gameService.PostChat(domainChat);

                return CreatedAtAction("GetChat", new { id = domainChat.Id }, domainChat);                            
            }
            catch
            {
                return NotFound();
            }

        }

        // DELETE: api/Games/5
        /// <summary>
        /// Deletes a game from the database based on an id provided by a parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns 204 No Content on success, returns 404 Not Found on fail</returns>
        [HttpDelete("{id}")]
        [Authorize("Admin")]
        public async Task<IActionResult> DeleteGame(int id)
        {
            try
            {
                var game = await _gameService.GetGameAsync(id);
                await _gameService.DeleteGame(game);

                return NoContent();
            }
            catch
            {
                return NotFound();
                
            }
        }
    }
}
