﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HvZ_API.Models;
using HvZ_API.Models.Domain;
using System.Net.Mime;
using AutoMapper;
using HvZ_API.Services.SquadService;
using HvZ_API.Models.DTO.Squad;
using HvZ_API.Models.DTO.Squad_Member;
using HvZ_API.Models.DTO.Chat;
using HvZ_API.Models.DTO.Squad_Checkin;
using Microsoft.AspNetCore.Authorization;

namespace HvZ_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Authorize]
    public class SquadsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ISquadService _squadService;

        public SquadsController(IMapper mapper, ISquadService squadService)
        {
            _mapper = mapper;
            _squadService = squadService;
        }

        // GET: api/Squads
        /// <summary>
        /// Gets all the squads with their respective squad members in a game from the database
        /// </summary>
        /// <returns>Returns a list of squads and their squad members</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SquadReadDTO>>> GetSquads()
        {
            var squads = _mapper.Map<List<SquadReadDTO>>(await _squadService.GetSquadsAsync());
            foreach(SquadReadDTO squad in squads)
            {
                var squadMembers = await _squadService.GetSquadMembersAsync(squad.Id);
                foreach(SquadMember squadMember in squadMembers)
                {
                    squad.SquadMembers.Add(squadMember.Id);
                }
            }
            return squads;
        }

        // GET: api/Squads/5
        /// <summary>
        /// Gets a squad based on an id provided by a parameter
        /// Adds the squad members with matching squadId's to an array within a squad
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns SquadReadDTO on success, 404 Not Found on fail</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<SquadReadDTO>> GetSquad(int id)
        {
            try
            {
                var squad = _mapper.Map<SquadReadDTO>(await _squadService.GetSquadAsync(id));
                var squadMembers = await _squadService.GetSquadMembersAsync(id);

                foreach(SquadMember squadMember in squadMembers)
                {
                    squad.SquadMembers.Add(squadMember.Id);
                }
                return squad;
            }
            catch
            {
                return NotFound();
            }
        }

        // GET: api/Squads/id/chat
        /// <summary>
        /// Gets a chat belonging to a squad from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns a list of chats on success, 404 Not Found on fail</returns>
        [HttpGet("{id}/chat")]
        public async Task<ActionResult<IEnumerable<ChatReadDTO>>> GetChat(int id)
        {
            try
            {
                var chat = _mapper.Map<List<ChatReadDTO>>(await _squadService.GetChat(id));
                return chat;
            }
            catch
            {
                return NotFound();
            }
        }

        // GET: api/Squads/squad_members
        /// <summary>
        /// Gets an array of all squadmembers in the database
        /// </summary>
        /// <returns>Returns a list on success, 404 Not found on fail</returns>
        [HttpGet("squad_members")]
        public async Task<ActionResult<IEnumerable<SquadMemberReadDTO>>> GetSquadMembers()
        {
            try
            {
                var squadmembers = _mapper.Map<List<SquadMemberReadDTO>>(await _squadService.GetAllSquadMembers());
                return squadmembers;
            }
            catch
            {
                return NotFound();
            }
        }

        //FUNKER IKKE I DET HELE TATT FAEN
        // GET: api/Squads/squad_member
        /// <summary>
        /// Gets a specific squad member based on an id provided by a parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns SquadReadDTO on success, 404 Not found on fail</returns>
        //[HttpGet("squad_member/{id}")]
        //public async Task<ActionResult<SquadReadDTO>> GetSquadMember(int id)
        //{
        //    try
        //    {
        //        var squadMember = _mapper.Map<SquadReadDTO>(await _squadService.GetSquadMemberAsync(id));
        //        return squadMember;
        //    }
        //    catch
        //    {
        //        return NotFound();
        //    }
        //}

        // GET: api/Squads/id/check-in
        /// <summary>
        /// Gets the squad check-ins from the database
        /// </summary>
        /// <returns>Returns a list of check-ins on success, 404 Not Found on fail</returns>
        [HttpGet("check-in")]
        public async Task<ActionResult<IEnumerable<SquadCheckinReadDTO>>> GetCheckin()
        {
            try
            {
                var checkin = _mapper.Map<List<SquadCheckinReadDTO>>(await _squadService.GetSquadCheckInsAsync());
                return checkin;
            }
            catch
            {
                return NotFound();
            }
        }

        // PUT: api/Squads/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Updates a squad based on id provided by a parameter
        /// </summary>
        /// <param name="id"></param>
        /// <param name="squad"></param>
        /// <returns>Returns 204 No Content on success, 400 Bad Request or 404 Not Found on fail</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSquad(int id, SquadEditDTO squad)
        {
            if (id != squad.Id)
            {
                return BadRequest();
            }

            if (!_squadService.SquadExists(id))
            {
                return NotFound();
            }

            Squad domainSquad = _mapper.Map<Squad>(squad);

            await _squadService.PutSquadAsync(domainSquad);

            return NoContent();
        }

        // POST: api/Squads
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Creates a new squad in the database
        /// </summary>
        /// <param name="squad"></param>
        /// <returns>Returns 204 No Cotent on success, 400 Bad Request or 404 Not Found on fail</returns>
        [HttpPost]
        public async Task<ActionResult<Squad>> PostSquad(SquadCreateDTO squad)
        {

            try
            {
                Squad domainSquad = _mapper.Map<Squad>(squad);
                await _squadService.PostSquadAsync(domainSquad);

                return CreatedAtAction("GetSquad", new { id = domainSquad.Id }, domainSquad);
            }
            catch
            {
                return NotFound();
            }
            
        }

        // POST: api/Squads/id/join
        /// <summary>
        /// Creates a new squad member in the database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="squadMember"></param>
        /// <returns>Returns 204 No Content on success, 400 Bad Request or 404 Not Found on fail</returns>
        [HttpPost("{id}/join")]
        public async Task<ActionResult<SquadMember>> PostSquadMember(int id, SquadMemberCreateDTO squadMember)
        {
            if (id != squadMember.SquadId)
            {
                return BadRequest();
            }
            try
            {
                try
                {
                    var squadmember = _mapper.Map<SquadMemberReadDTO>(await _squadService.GetSquadMemberAsync(squadMember.PlayerId));

                    await this.DeleteSquadMember(squadmember.Id);

                    SquadMember domainMember = _mapper.Map<SquadMember>(squadMember);
                    await _squadService.PostSquadMemberAsync(domainMember);
                    return CreatedAtAction("GetSquad", new { id = domainMember.Id }, domainMember);
                }
                catch
                {
                    SquadMember domainMember = _mapper.Map<SquadMember>(squadMember);
                    await _squadService.PostSquadMemberAsync(domainMember);
                    return CreatedAtAction("GetSquad", new { id = domainMember.Id }, domainMember);
                }
            }
            catch
            {
                return NotFound();
            }
            
        }

        // POST: api/Squads/id/chat
        /// <summary>
        /// Creates a new chat belonging to a squad in the database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="chat"></param>
        /// <returns>Returns 204 No Content on success, 400 Bad Request or 404 Not Found on fail</returns>
        [HttpPost("{id}/chat")]
        public async Task<ActionResult<Chat>> PostChat(int id, ChatCreateDTO chat)
        {
            if (id != chat.SquadId)
            {
                return BadRequest();
            }
            try
            {
                Chat domainChat = _mapper.Map<Chat>(chat);
                await _squadService.PostSquadChat(domainChat);

                return CreatedAtAction("GetSquad", new { id = domainChat.Id }, domainChat);
            }
            catch
            {
                return NotFound();
            }
            
        }

        // POST: api/Squads/id/check-in
        /// <summary>
        /// Creates a check-in for a squad member in a squad, and saves it to the database
        /// If there already exists a check-in for the squad member it is deleted and a new one is set
        /// </summary>
        /// <param name="id"></param>
        /// <param name="checkin"></param>
        /// <returns>Returns 204 No Content on success, 400 Bad Request or 404 Not Found on fail</returns>
        [HttpPost("{id}/check-in")]
        public async Task<ActionResult<SquadCheckin>> PostCheckin(int id, SquadCheckinCreateDTO checkin)
        {
            if (id != checkin.SquadId)
            {
                return BadRequest();
            }
            try
            {
                SquadCheckin squadCheckin = await _squadService.GetSquadCheckInAsync(checkin.SquadMemberId);
                await _squadService.DeleteSquadCheckIn(squadCheckin);

                SquadCheckin domainCheckin = _mapper.Map<SquadCheckin>(checkin);
                await _squadService.PostSquadCheckInAsync(domainCheckin);

                return CreatedAtAction("GetSquad", new { id = domainCheckin.Id }, domainCheckin);
            }
            catch
            {
                try
                {
                    SquadCheckin domainCheckin = _mapper.Map<SquadCheckin>(checkin);
                    await _squadService.PostSquadCheckInAsync(domainCheckin);

                    return CreatedAtAction("GetSquad", new { id = domainCheckin.Id }, domainCheckin);
                }
                catch
                {
                    return NotFound();
                }
                
            }           
        }

        // DELETE: api/Squads/5
        /// <summary>
        /// Deletes a squad in the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns 204 No Content on success, returns 404 Not Found on fail</returns>
        [HttpDelete("{id}")]
        [Authorize("Admin")]
        public async Task<IActionResult> DeleteSquad(int id)
        {
            try
            {
                var squad = await _squadService.GetSquadAsync(id);
                await _squadService.DeleteSquad(squad);

                return NoContent();
            }
            catch
            {
                return NotFound();
            }

        }

        // DELETE: api/Squads/5
        /// <summary>
        /// Deletes a squad member in the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("SquadMember/{id}")]
        public async Task<IActionResult> DeleteSquadMember(int id)
        {
            try
            {
                
                try
                {
                    var squadMember = await _squadService.GetSquadMemberAsync(id);
                    SquadCheckin squadCheckin = await _squadService.GetSquadCheckInAsync(squadMember.Id);
                    await _squadService.DeleteSquadCheckIn(squadCheckin);

                    
                    await _squadService.DeleteSquadMember(squadMember);

                    return NoContent();
                }
                catch
                {
                    var squadMember = await _squadService.GetSquadMemberAsync(id);
                    await _squadService.DeleteSquadMember(squadMember);

                    return NoContent();
                }
            }
            catch
            {
                return NotFound();
            }

        }

        [HttpDelete("SquadMember/checkin/{id}")]
        public async Task<IActionResult> DeleteSquadCheckin(int id)
        {
            try
            {
                SquadCheckin squadCheckin = await _squadService.GetSquadCheckInAsync(id);
                await _squadService.DeleteSquadCheckIn(squadCheckin);

                return NoContent();
            }
            catch
            {
                return NotFound();
            }
        }

    }
}
