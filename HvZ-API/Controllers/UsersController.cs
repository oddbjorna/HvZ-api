﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HvZ_API.Models;
using HvZ_API.Models.Domain;
using System.Net.Mime;
using AutoMapper;
using HvZ_API.Services.UserService;
using HvZ_API.Models.DTO.User;
using Microsoft.AspNetCore.Authorization;

namespace HvZ_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Authorize]
    public class UsersController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        public UsersController(IMapper mapper, IUserService userService)
        {
            _mapper = mapper;
            _userService = userService;
        }

        // GET: api/Users
        /// <summary>
        /// Gets all the users in the database
        /// </summary>
        /// <returns>Returns a list of users</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetUsers()
        {
            return _mapper.Map<List<UserReadDTO>>(await _userService.GetUsersAsync());
        }

        // GET: api/Users/5
        /// <summary>
        /// Gets a specific user based on id provided by a parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns UserReadDTO on success, returns 404 Not Found on fail</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<UserReadDTO>> GetUser(int id)
        {
            try
            {
                var user = _mapper.Map<UserReadDTO>(await _userService.GetUserAsync(id));
                return user;
            }
            catch
            {
                return NotFound();
            }
        }



        // POST: api/Users
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Creates a new user in the database
        /// </summary>
        /// <param name="user"></param>
        /// <returns>Returns 201 Created user</returns>
        [HttpPost]
        public async Task<ActionResult<User>> PostUser(UserCreateDTO user)
        {
            User domainUser = _mapper.Map<User>(user);
            await _userService.PostUserAsync(domainUser);

            return CreatedAtAction("GetUser", new { id = domainUser.Id }, domainUser);
        }

        // DELETE: api/Users/5
        //[HttpDelete("{id}")]
        //public async Task<IActionResult> DeleteUser(int id)
        //{
        //    var user = await _context.Users.FindAsync(id);
        //    if (user == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.Users.Remove(user);
        //    await _context.SaveChangesAsync();

        //    return NoContent();
        //}

        //private bool UserExists(int id)
        //{
        //    return _context.Users.Any(e => e.Id == id);
        //}
    }
}
