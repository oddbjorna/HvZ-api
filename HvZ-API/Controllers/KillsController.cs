﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HvZ_API.Models;
using HvZ_API.Models.Domain;
using System.Net.Mime;
using AutoMapper;
using HvZ_API.Services.KillService;
using HvZ_API.Models.DTO.Kill;
using Microsoft.AspNetCore.Authorization;

namespace HvZ_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Authorize]
    public class KillsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IKillService _killService;

        public KillsController(IMapper mapper, IKillService killService)
        {
            _mapper = mapper;
            _killService = killService;
        }

        // GET: api/Kills
        /// <summary>
        /// Gets all the kills in the database
        /// </summary>
        /// <returns>Returns a list of kills</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<KillReadDTO>>> GetKills()
        {
            return _mapper.Map<List<KillReadDTO>>(await _killService.GetKillsAsync());
        }

        // GET: api/Kills/5
        /// <summary>
        /// Gets a specific kill based on an id provided by a parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns KillReadDTO on success, 404 Not found on fail</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<KillReadDTO>> GetKill(int id)
        {
            try
            {
                var kill = _mapper.Map<KillReadDTO>(await _killService.GetKillAsync(id));
                return kill;
            } catch
            {
                return NotFound();
            }
            
        }

        // PUT: api/Kills/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Updates the attributes of an existing kill based on an id provided by a parameter.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="kill"></param>
        /// <returns>Returns 204 No Content on success, 400 Bad Request or 404 Not Found on fail</returns>
        [HttpPut("{id}")]
        [Authorize("Admin")]
        public async Task<IActionResult> PutKill(int id, KillEditDTO kill)
        {
            if (id != kill.Id)
            {
                return BadRequest();
            }

            if (!_killService.KillExists(id))
            {
                return NotFound();
            }

            Kill domainKill = _mapper.Map<Kill>(kill);

            await _killService.PutKillAsync(domainKill);

            return NoContent();
        }

        // POST: api/Kills
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Creates a new kill in the database
        /// </summary>
        /// <param name="kill"></param>
        /// <returns>Returns 201 Created kill, returns 404 Not Found on fail</returns>
        [HttpPost]
        public async Task<ActionResult<Kill>> PostKill(KillCreateDTO kill)
        {
            try
            {
                Kill domainKill = _mapper.Map<Kill>(kill);
                await _killService.PostKillAsync(domainKill);

                return CreatedAtAction("GetKill", new { id = domainKill.Id }, domainKill);
            }
            catch
            {
                return NotFound();
            }       
        }

        // DELETE: api/Kills/5
        /// <summary>
        /// Deletes a kill from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns 204 No Content, returns 404 Not Found on fail</returns>
        [HttpDelete("{id}")]
        [Authorize("Admin")]
        public async Task<IActionResult> DeleteKill(int id)
        {
            try
            {
                var kill = await _killService.GetKillAsync(id);
                await _killService.DeleteKill(kill);

                return NoContent();
            } catch
            {
                return NotFound();
            }
            
        }

    }
}
