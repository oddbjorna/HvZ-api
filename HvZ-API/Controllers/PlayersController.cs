﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HvZ_API.Models;
using HvZ_API.Models.Domain;
using System.Net.Mime;
using AutoMapper;
using HvZ_API.Services.PlayerService;
using HvZ_API.Models.DTO.Player;
using Microsoft.AspNetCore.Authorization;

namespace HvZ_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Authorize]
    public class PlayersController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IPlayerService _playerService;

        public PlayersController(IMapper mapper, IPlayerService playerService)
        {
            _mapper = mapper;
            _playerService = playerService;
        }

        // GET: api/Players
        /// <summary>
        /// Gets all the players from the database
        /// </summary>
        /// <returns>Returns a list of players</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PlayerReadDTO>>> GetPlayers()
        {
            return _mapper.Map<List<PlayerReadDTO>>(await _playerService.GetPlayersAsync());
        }

        // GET: api/Players/5
        /// <summary>
        /// Gets a specific player based on an id provided by a parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns PlayerReadDTO on success, 404 Not found on fail</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<PlayerReadDTO>> GetPlayer(int id)
        {
            try
            {
                var player = _mapper.Map<PlayerReadDTO>(await _playerService.GetPlayerAsync(id));
                return player;
            }
            catch
            {
                return NotFound();
            }
        }

        // PUT: api/Players/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Updates the attributes of an existing player based on an id provided by a parameter
        /// </summary>
        /// <param name="id"></param>
        /// <param name="player"></param>
        /// <returns>Returns 204 No Content on success, 400 Bad Request or 404 Not Found on fail</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPlayer(int id, PlayerEditDTO player)
        {
            if (id != player.Id)
            {
                return BadRequest();
            }

            if (!_playerService.PlayerExists(id))
            {
                return NotFound();
            }

            Player domainPlayer = _mapper.Map<Player>(player);

            await _playerService.PutPlayerAsync(domainPlayer);

            return NoContent();
        }

        // POST: api/Players
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Creates a new player in the database
        /// </summary>
        /// <param name="player"></param>
        /// <returns>Returns 201 Created player. Returns 404 Not Found on fail</returns>
        [HttpPost]
        public async Task<ActionResult<Player>> PostPlayer(PlayerCreateDTO player)
        {
            try
            {
                Player domainPlayer = _mapper.Map<Player>(player);
                await _playerService.PostPlayerAsync(domainPlayer);

                return CreatedAtAction("GetPlayer", new { id = domainPlayer.Id }, domainPlayer);
            }
            catch
            {
                return NotFound();
            }
            
        }

        // DELETE: api/Players/5
        /// <summary>
        /// Deletes a player in the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns 204 No Content, returns 404 Not Found on fail</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePlayer(int id)
        {
            try
            {
                var player = await _playerService.GetPlayerAsync(id);
                await _playerService.DeletePlayer(player);

                return NoContent();
            }
            catch
            {
                return NotFound();
            }
        }        
    }
}
