﻿using AutoMapper;
using HvZ_API.Models.Domain;
using HvZ_API.Models.DTO.Squad_Checkin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Profiles
{
    public class SquadCheckinProfile : Profile
    {
        public SquadCheckinProfile()
        {
            CreateMap<SquadCheckin, SquadCheckinReadDTO>();
            CreateMap<SquadCheckinCreateDTO, SquadCheckin>();

        }

    }
}
