﻿using AutoMapper;
using HvZ_API.Models.Domain;
using HvZ_API.Models.DTO.Kill;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Profiles
{
    public class KillProfile : Profile
    {
        public KillProfile()
        {
            CreateMap<Kill, KillReadDTO>();
            CreateMap<KillCreateDTO, Kill>();
            CreateMap<KillEditDTO, Kill>();
        }
    }
}
