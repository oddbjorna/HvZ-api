﻿using AutoMapper;
using HvZ_API.Models.Domain;
using HvZ_API.Models.DTO.Mission;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Profiles
{
    public class MissionProfile : Profile
    {
        public MissionProfile()
        {
            CreateMap<Mission, MissionReadDTO>();
            CreateMap<MissionCreateDTO, Mission>();
            CreateMap<MissionEditDTO, Mission>();
        }
    }
}
