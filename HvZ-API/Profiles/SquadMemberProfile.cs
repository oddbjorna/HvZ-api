﻿using AutoMapper;
using HvZ_API.Models.Domain;
using HvZ_API.Models.DTO.Squad_Member;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Profiles
{
    public class SquadMemberProfile : Profile
    {
        public SquadMemberProfile()
        {
            CreateMap<SquadMember, SquadMemberReadDTO>();
            CreateMap<SquadMemberCreateDTO, SquadMember>();
        }

    }
}
