﻿using AutoMapper;
using HvZ_API.Models.Domain;
using HvZ_API.Models.DTO.Squad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Profiles
{
    public class SquadProfile : Profile
    {
        public SquadProfile()
        {
            CreateMap<Squad, SquadReadDTO>();
            CreateMap<SquadCreateDTO, Squad>();
            CreateMap<SquadEditDTO, Squad>();

        }
    }
}
