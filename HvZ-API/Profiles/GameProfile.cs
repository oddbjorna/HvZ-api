﻿using AutoMapper;
using HvZ_API.Models.Domain;
using HvZ_API.Models.DTO.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Profiles
{
    public class GameProfile : Profile
    {
        public GameProfile()
        {
            CreateMap<Game, GameReadDTO>();
            CreateMap<GameCreateDTO, Game>();
            CreateMap<GameEditDTO, Game>();

        }
    }
}
