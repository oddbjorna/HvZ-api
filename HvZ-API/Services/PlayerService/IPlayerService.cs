﻿using System;
using HvZ_API.Models.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Services.PlayerService
{
    public interface IPlayerService
    {
        // GET /game/<game_id>/player
        public Task<IEnumerable<Player>> GetPlayersAsync();
        // GET /game/<game_id>/player/<player_id>
        public Task<Player> GetPlayerAsync(int id);
        // POST /game/<game_id>/player
        public Task PostPlayerAsync(Player player);
        // PUT /game/<game_id>/player/<player_id>
        public Task PutPlayerAsync(Player player);
        // DELETE /game/<game_id>/player/<player_id>
        public Task DeletePlayer(Player player);
        public bool PlayerExists(int id);
    }
}
