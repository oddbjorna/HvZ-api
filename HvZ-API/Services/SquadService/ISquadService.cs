﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HvZ_API.Models.Domain;

namespace HvZ_API.Services.SquadService
{
    public interface ISquadService
    {
        public Task<IEnumerable<Squad>> GetSquadsAsync();
        public Task<Squad> GetSquadAsync(int id);
        public Task PostSquadAsync(Squad squad);
        public Task PostSquadMemberAsync(SquadMember squadMember);
        public Task PutSquadAsync(Squad squad);
        public Task DeleteSquad(Squad squad);
        public Task<IEnumerable<Chat>> GetChat(int id);
        public Task PostSquadChat(Chat chat);
        public Task<IEnumerable<SquadCheckin>> GetSquadCheckInsAsync();
        public Task <SquadCheckin> GetSquadCheckInAsync(int id);
        public Task PostSquadCheckInAsync(SquadCheckin squadCheckin);
        public Task DeleteSquadCheckIn(SquadCheckin squadCheckin);
        public Task<IEnumerable<SquadMember>> GetAllSquadMembers();
        public Task<IEnumerable<SquadMember>> GetSquadMembersAsync(int id);
        public Task<SquadMember> GetSquadMemberAsync(int id);
        public Task DeleteSquadMember(SquadMember squadMember);
        public bool SquadExists(int id);
    }
}
