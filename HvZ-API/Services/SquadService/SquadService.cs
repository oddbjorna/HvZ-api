﻿using HvZ_API.Models;
using HvZ_API.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace HvZ_API.Services.SquadService
{
    public class SquadService : ISquadService
    {
        private readonly HvZDbContext _context;
        public SquadService(HvZDbContext context)
        {
            _context = context;
        }
        public async Task DeleteSquad(Squad squad)
        {
            _context.Squads.Remove(squad);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Chat>> GetChat(int id)
        {
            return await _context.Chats.Where(c => c.SquadId == id).ToListAsync();
        }

        public async Task<Squad> GetSquadAsync(int id)
        {
            return await _context.Squads.Where(s => s.Id == id).FirstAsync();
        }

        public async Task<IEnumerable<SquadCheckin>> GetSquadCheckInsAsync()
        {
            return await _context.Squad_Checkins.ToListAsync();
        }

        public async Task<SquadCheckin> GetSquadCheckInAsync(int id)
        {
            return await _context.Squad_Checkins.Where(c => c.SquadMemberId == id).FirstAsync();
        }

        public async Task<IEnumerable<Squad>> GetSquadsAsync()
        {
            return await _context.Squads.ToListAsync();
        }

        public async Task PostSquadAsync(Squad squad)
        {
            _context.Squads.Add(squad);
            await _context.SaveChangesAsync();
        }

        public async Task PostSquadChat(Chat chat)
        {
            _context.Chats.Add(chat);
            await _context.SaveChangesAsync();
        }

        public async Task PostSquadCheckInAsync(SquadCheckin squadCheckin)
        {
            _context.Squad_Checkins.Add(squadCheckin);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteSquadCheckIn(SquadCheckin squadCheckin)
        {
            _context.Squad_Checkins.Remove(squadCheckin);
            await _context.SaveChangesAsync();
        }

        public async Task PostSquadMemberAsync(SquadMember squadMember)
        {
            _context.Squad_Members.Add(squadMember);
            await _context.SaveChangesAsync();
        }

        public async Task PutSquadAsync(Squad squad)
        {
            _context.Entry(squad).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        
        public async Task<IEnumerable<SquadMember>> GetAllSquadMembers()
        {
            return await _context.Squad_Members.ToListAsync();
        }

        public async Task<IEnumerable< SquadMember>> GetSquadMembersAsync(int id)
        {
            return await _context.Squad_Members.Where(c => c.SquadId == id).ToListAsync();
        }

        public async Task<SquadMember> GetSquadMemberAsync(int id)
        {
            return await _context.Squad_Members.Where(c => c.PlayerId == id).FirstAsync();
        }

        public async Task DeleteSquadMember(SquadMember squadMember)
        {
            _context.Squad_Members.Remove(squadMember);
            await _context.SaveChangesAsync();
        }

        public bool SquadExists(int id)
        {
            return _context.Squads.Any(e => e.Id == id);
        }
    }
}
