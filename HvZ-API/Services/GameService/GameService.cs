﻿using HvZ_API.Models;
using HvZ_API.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Services.GameService
{
    public class GameService : IGameService
    {
        private readonly HvZDbContext _context;

        public GameService(HvZDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Game>> GetGamesAsync()
        {
            return await _context.Games.ToListAsync();
        }

        public async Task<Game> GetGameAsync(int id)
        {
            return await _context.Games.Where(g => g.Id == id).FirstAsync();
        }

        public async Task PutGameAsync(Game game)
        {
            _context.Entry(game).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task PostGameAsync(Game game)
        {
            _context.Games.Add(game);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteGame(Game game)
        {
            _context.Games.Remove(game);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Chat>> GetChat(int id)
        {
            return await _context.Chats.Where(c => c.GameId == id && c.SquadId == null && c.IsHumanGlobal == false && c.IsZombieGlobal == false).ToListAsync();
        }

        public async Task<IEnumerable<Chat>> GetHumanFactionChat(int id)
        {
            return await _context.Chats.Where(c => c.GameId == id && c.SquadId == null && c.IsHumanGlobal == true && c.IsZombieGlobal == false).ToListAsync();
        }

        public async Task<IEnumerable<Chat>> GetZombieFactionChat(int id)
        {
            return await _context.Chats.Where(c => c.GameId == id && c.SquadId == null && c.IsHumanGlobal == false && c.IsZombieGlobal == true).ToListAsync();
        }

        public async Task PostChat(Chat chat)
        {
            _context.Chats.Add(chat);
            await _context.SaveChangesAsync();
        }

        public bool GameExists(int id)
        {
            return _context.Games.Any(g => g.Id == id);
        }
    }
}
