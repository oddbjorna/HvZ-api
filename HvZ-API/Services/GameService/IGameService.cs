﻿using System;
using HvZ_API.Models.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Services.GameService
{
    public interface IGameService
    {
        public Task<IEnumerable<Game>> GetGamesAsync();
        public Task<Game> GetGameAsync(int id);
        public Task PutGameAsync(Game game);
        public Task PostGameAsync(Game game);
        public Task DeleteGame(Game game);
        public Task<IEnumerable<Chat>> GetChat(int id);
        public Task<IEnumerable<Chat>> GetHumanFactionChat(int id);
        public Task<IEnumerable<Chat>> GetZombieFactionChat(int id);
        public Task PostChat(Chat chat);
        public bool GameExists(int id);
    }
}
