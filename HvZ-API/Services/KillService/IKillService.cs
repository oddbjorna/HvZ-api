﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HvZ_API.Models.Domain;

namespace HvZ_API.Services.KillService
{
    public interface IKillService
    {
        public Task<IEnumerable<Kill>> GetKillsAsync();

        public Task<Kill> GetKillAsync(int id);

        public Task PostKillAsync(Kill kill);

        public Task PutKillAsync(Kill kill);

        public Task DeleteKill(Kill kill);

        public bool KillExists(int id);
    }
}