﻿using HvZ_API.Models;
using HvZ_API.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Services.KillService
{
    public class KillService : IKillService
    {
        private readonly HvZDbContext _context;

        public KillService(HvZDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Kill>> GetKillsAsync()
        {
            return await _context.Kills.ToListAsync();
        }

        public async Task<Kill> GetKillAsync(int id)
        {
            return await _context.Kills.Where(k => k.Id == id).FirstAsync();
        }

        public async Task PostKillAsync(Kill kill)
        {
            _context.Kills.Add(kill);
            await _context.SaveChangesAsync();
        }

        public async Task PutKillAsync(Kill kill)
        {
            _context.Entry(kill).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteKill(Kill kill)
        {
            _context.Kills.Remove(kill);
            await _context.SaveChangesAsync();
        }

        public bool KillExists(int id)
        {
            return _context.Kills.Any(k => k.Id == id);
        }

    }
}