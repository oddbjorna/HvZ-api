﻿using HvZ_API.Models;
using HvZ_API.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace HvZ_API.Services.MissionService
{
    public class MissionService : IMissionService
    {
        private readonly HvZDbContext _context;

        public MissionService(HvZDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Mission>> GetMissionsAsync()
        {
            return await _context.Missions.ToListAsync();
        }

        public async Task<Mission> GetMissionAsync(int id)
        {
            return await _context.Missions.Where(g => g.Id == id).FirstAsync();
        }

        public async Task PutMissionAsync(Mission mission)
        {
            _context.Entry(mission).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task PostMissionAsync(Mission mission)
        {
            _context.Missions.Add(mission);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteMission(Mission mission)
        {
            _context.Missions.Remove(mission);
            await _context.SaveChangesAsync();
        }

        public bool MissionExists(int id)
        {
            return _context.Missions.Any(e => e.Id == id);
        }

    }
}
