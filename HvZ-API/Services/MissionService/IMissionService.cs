﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HvZ_API.Models.Domain;


namespace HvZ_API.Services.MissionService
{
    public interface IMissionService
    {
        public Task<IEnumerable<Mission>> GetMissionsAsync();
        public Task<Mission> GetMissionAsync(int id);
        public Task PutMissionAsync(Mission mission);
        public Task PostMissionAsync(Mission mission);
        public Task DeleteMission(Mission mission);
        public bool MissionExists(int id);
    }
}
