﻿using HvZ_API.Hubs.Models;
using HvZ_API.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Hubs.Clients
{
    public interface IGameClient
    {
        Task ReceiveMessage(ChatMessage message);
        Task ReceiveMarker(CheckinMarker marker);
        Task ReceiveKill(KillMarker marker);
        Task ReceiveMission(MissionMarker marker);
    }
}
