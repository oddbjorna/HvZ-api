﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Hubs.Models
{
    public class ChatMessage
    {
        public string Message { get; set; }
    }
}
