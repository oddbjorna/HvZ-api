﻿using HvZ_API.Hubs.Clients;
using HvZ_API.Hubs.Models;
using HvZ_API.Models.Domain;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HvZ_API.Hubs
{
    public class GameHub : Hub<IGameClient>
    {
        public async Task SendMessage(ChatMessage message)
        {
            await Clients.All.ReceiveMessage(message);
        }
        public async Task SendCheckin(CheckinMarker marker)
        {
            await Clients.All.ReceiveMarker(marker);
        }
        public async Task SendKill(KillMarker marker)
        {
            await Clients.All.ReceiveKill(marker);
        }
        public async Task SendMission(MissionMarker marker)
        {
            await Clients.All.ReceiveMission(marker);
        }
    }
}
